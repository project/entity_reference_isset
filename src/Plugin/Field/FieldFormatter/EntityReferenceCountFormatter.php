<?php

namespace Drupal\entity_reference_isset\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity_reference_count' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_count",
 *   label = @Translation("Entity reference count"),
 *   description = @Translation("Count of entity reference items set in the field."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceCountFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();
    $options['suffix'] = '';
    $options['suffix_plural'] = '';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix'),
      '#default_value' => $this->getSetting('suffix'),
    ];

    $form['suffix_plural'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix plural'),
      '#default_value' => $this->getSetting('suffix_plural'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $value = $items->count();

    if ($suffix = $this->getSetting('suffix')) {
      if (!$plural = $this->getSetting('suffix_plural')) {
        $plural = $suffix;
      }
      $args = [
        '@suffix' => $suffix,
        '@plural' => $plural,
      ];
      $suffix = "1 @suffix";
      $plural = "@count @plural";
      $value = $this->formatPlural($value, $suffix, $plural, $args);
    }

    $elements[0] = [
      '#markup' => Html::escape($value),
    ];

    return $elements;
  }

}
