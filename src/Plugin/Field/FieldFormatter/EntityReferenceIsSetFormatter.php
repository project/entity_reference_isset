<?php

namespace Drupal\entity_reference_isset\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity_reference_isset' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_isset",
 *   label = @Translation("Entity reference is set"),
 *   description = @Translation("Indicates if entity reference items are set in the field."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceIsSetFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();
    $options['true_value'] = 'Yes';
    $options['false_value'] = 'No';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['true_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('True value'),
      '#default_value' => $this->getSetting('true_value'),
      '#required' => TRUE,
    ];

    $form['false_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('False value'),
      '#default_value' => $this->getSetting('false_value'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    if ($items->isEmpty()) {
      $value = $this->getSetting('false_value');
    }
    else {
      $value = $this->getSetting('true_value');
    }

    $elements[0] = [
      '#markup' => Html::escape($value),
    ];

    return $elements;
  }

}
